<?php
/**
 * @file
 * uaqs_content_chunks_marquee.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uaqs_content_chunks_marquee_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_uaqs_setting_bg_attach'.
  $field_bases['field_uaqs_setting_bg_attach'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_uaqs_setting_bg_attach',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'bg-attachment-fixed' => 'Fixed',
        'bg-attachment-scroll' => 'Scroll',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_uaqs_setting_bg_size'.
  $field_bases['field_uaqs_setting_bg_size'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_uaqs_setting_bg_size',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'bg-size-100-w' => 'Scale to 100% width',
        'bg-size-cover' => 'Scale to cover viewport',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_uaqs_setting_text_bg_color'.
  $field_bases['field_uaqs_setting_text_bg_color'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_uaqs_setting_text_bg_color',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'bg-trans-white' => 'Transluscent white',
        'bg-trans-sky' => 'Transluscent sky',
        'bg-trans-arizona-blue' => 'Transluscent blue',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
