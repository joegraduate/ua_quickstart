/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.uaqs_alphabetical_search = {
    attach: function (context, settings) {
      function enableLinks() {
        $("#uaqs-alpha-navigation li", context).each(function(i, li) {
          var $group_id = $(this).children().attr('href');
          if($($group_id).length != 0 && !$($group_id).hasClass('element-invisible')) {
            $(this).removeClass('disabled');
            $(this).children().attr('tabindex','0');
          }
          else {
            $(this).addClass('disabled');
            $(this).children().attr('tabindex','-1');
          }
        });
      }
      /**
       * Helper function that returns a boolean if the subject matches the query
       * @param subject
       * @param query
       * @returns {boolean}
       */
      function isMatch(subject, query) {
        subject = subject.toLowerCase();
        query = query.toLowerCase();

        if (query.indexOf(subject) != -1) {
          return true;
        } else {
          return false;
        }
      }

      /**
       * Searches the visible content in the .uaqs-search-result containers.
       *
       * @param subject : string that has been entered by the user.
       */
      function searchAlphabeticList(subject) {
        $('.uaqs-search-result').children(':visible').each(function () {
          var match = isMatch(subject, $(this).text());
          // Hide the result if it's not a match.
          if (!match) {
            $(this).parents('.uaqs-search-result').removeClass('uaqs-row-visible').addClass('element-invisible');
            $(this).children('a').attr('tabindex','-1')
            checkParent();
          }
          // Allow navigation if is a match.
          if (match) {
            $(this).children('a').attr('tabindex','0')
          }
        });
      }

      /**
       * Checks if a parent has no visible children. If that's the case, hide the whole section.
       */
      function checkParent() {
        if ($('.uaqs-letter-container').find('.element-invisible').length) {
          var uaqsLetterContainer = $('.uaqs-letter-container:not(:has(.uaqs-row-visible))');
          uaqsLetterContainer.addClass('element-invisible');
          // TODO disable the alpha navigation links that are no longer referencing visible sections.
          //console.log('#' + uaqsLetterContainer.attr('id'));
          uaqsLetterContainerID = '#' + uaqsLetterContainer.attr('id');
          $('a[href="' + uaqsLetterContainerID +'"]').parent().addClass('disabled');
          enableLinks();
        }
      }

      /**
       *  If everything is hidden, display the no results text.
       */
      function checkNoResults() {
        if ($('.uaqs-letter-container:not(.element-invisible)').length == 0) {
          $('.uaqs-no-results').show();
        } else {
          $('.uaqs-no-results').hide();
        }
      }

      /**
       * Resets the CSS class that hides elements.
       */
      function reset() {
          $('.view-uaqs-alphabetical-listing .element-invisible').removeClass('element-invisible').addClass('uaqs-row-visible');
      }


      /**
       * When the user changes the input, update the results.
       */
      $('.local-search input').on('input', function () {
          reset();
          searchAlphabeticList($(this).val());
          checkNoResults();
          enableLinks();
      });

      enableLinks();
    }
  }

  // Smooth scrolling for jump links.
  Drupal.behaviors.smooth_alpha_jump = {
    attach: function (context, settings) {
      var $root = $('html, body');
      var breakpoint = 600;

      $('#uaqs-alpha-navigation a').on('click', function(event){
        event.preventDefault();
        var $alpha_nav = $('#uaqs-floating-alpha-nav', context);
        var href = $.attr(this, 'href');
        href = href.replace('_label', '');
        var $jump = $('div[id=' + href.substring(1) + ']');
        var fixed_nav_height = $alpha_nav.outerHeight();
        if ($(window).width() <= breakpoint) {
          fixed_nav_height = 0;
        }
        if (!$alpha_nav.hasClass('affix')) {
          fixed_nav_height *= 2;
        }
        $root.animate({
          scrollTop: $jump.offset().top - fixed_nav_height
        }, 500, function () {
          window.location.hash = href + '_label';
        });
      });
    }
  };

  $(window).bind('load', function() {
    var top = $('.view-uaqs-alphabetical-listing').offset().top
    var navHeight = $('#uaqs-floating-alpha-nav').innerHeight()
    var innerHeight = $('.view-uaqs-alphabetical-listing').innerHeight()
    var outerHeight = $('body').outerHeight()
    $('#uaqs-floating-alpha-nav-container').css('height', navHeight);
    var offsetBottom = outerHeight - innerHeight - top;
    var offsetTop = top;
    $('#uaqs-floating-alpha-nav').affix({
        offset: { top: offsetTop, bottom: offsetBottom }
    })
  });

})(jQuery, Drupal, this, this.document);
