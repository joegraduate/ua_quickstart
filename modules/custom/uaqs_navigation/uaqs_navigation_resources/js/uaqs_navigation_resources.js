(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.hover_resources = {
    attach: function (context, settings) {
      $('#block-bean-uaqs-resources-bean').hover(
        function() {
          $(this).addClass('open');
        },
        function() {
          $(this).removeClass('open');
        }
      );
    }
  };

  Drupal.behaviors.copy_resources_nav = {
    attach: function (context, settings) {
      if( $(window).width() < 991 ) {
        // Copy the resources nav into the main nav on page load since we need the menu in two locations.
        var navItemMarkup = '<li class="menu__item menu__item-resources is-expanded last expanded dropdown"><a href="#" class="nav__link" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">RESOURCES <span class="caret"></span></a></li>';
        $('#navbar ul.menu').append(navItemMarkup);
        $('#block-bean-uaqs-resources-bean ul.dropdown-menu').clone().appendTo($('.menu__item-resources'));
      }
    }
  };

})(jQuery, Drupal, this, this.document);
