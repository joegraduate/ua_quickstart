<?php
/**
 * @file
 * Add content to demonstrate a UAQS Block Types - Call To Action Bean.
 */

/**
 * The field contents come from a JSON file.
 */
class UaqsBlockTypesCallToActionBeanMigration extends UaqsPlacedBeanMigration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments, 'uaqs_call_to_action', 'ua_zen', 'content', BLOCK_VISIBILITY_LISTED, '*', 1,
      t('Make Call To Action Bean Block content from pre-defined data.'));

    // Documented lists of source data fields.
    // See uaqs_block_types.features.field_instance.inc.
    $data_fields = array(
      'label' => t('Descriptive title that the administrative interface uses'),
      'title' => t('Title'),
      'data' => t('Data'),
      'uaqs_link' => t('Link')
    );

    // Titles for links...
    $link_title_fields = array(
      'uaqs_link_title' => t('Link title'),
    );

    // Attributes for links...
    $link_attributes_fields = array(
      'uaqs_link_attributes' => t('Link attributes'),
    );

    // All the fields to migrate.
    $fields = $this->getSourceKeyField() + $data_fields + $link_title_fields + $link_attributes_fields;

    // Source definition.
    $this->source = new MigrateSourceJSON($this->jsonFilePath(),
      $this->getSourceKeyFieldName(), $fields);

    // JSON names to fields mappings.
    $this->addSimpleMappings(array('label'));

    // The title has no prefix.
    $this->addSimpleMappings(array('title'));

    // JSON names to fields mappings.
    $this->addSimpleMappings(array('data'));

    $this->addFieldMapping('field_uaqs_link', 'uaqs_link');
    $this->addFieldMapping('field_uaqs_link:title', 'uaqs_link_title');
    $this->addFieldMapping('field_uaqs_link:attributes', 'uaqs_link_attributes');
  }

  public function prepareRow($row) {
    $row->data = json_decode(json_encode($row->data), JSON_OBJECT_AS_ARRAY);
    $row->uaqs_link_attributes = (array) $row->uaqs_link_attributes;
    return TRUE;
  }
}
