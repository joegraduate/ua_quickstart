<?php
/**
 * @file
 * Custom buttons for embedding entities.
 */

/**
 * Implements hook_default_entity_embed_configuration().
 */
function uaqs_fields_default_entity_embed_configuration() {
  $export = array();

  $configuration = new stdClass();
  $configuration->disabled = FALSE; /* Edit this to true to make a default configuration disabled initially */
  $configuration->api_version = 1;
  $configuration->admin_title = 'Block (Bean)';
  $configuration->name = 'uaqs_bean';
  $configuration->entity_type = 'bean';
  $configuration->entity_type_bundles = array();
  $configuration->button_icon_fid = 0;
  $configuration->display_plugins = array(
    'entityreference:entityreference_entity_view' => 'entityreference:entityreference_entity_view',
  );
  $export['uaqs_bean'] = $configuration;

  $configuration = new stdClass();
  $configuration->disabled = FALSE; /* Edit this to true to make a default configuration disabled initially */
  $configuration->api_version = 1;
  $configuration->admin_title = 'Content';
  $configuration->name = 'uaqs_content';
  $configuration->entity_type = 'node';
  $configuration->entity_type_bundles = array();
  $configuration->button_icon_fid = 0;
  $configuration->display_plugins = array();
  $export['uaqs_content'] = $configuration;

  $configuration = new stdClass();
  $configuration->disabled = FALSE; /* Edit this to true to make a default configuration disabled initially */
  $configuration->api_version = 1;
  $configuration->admin_title = 'Files';
  $configuration->name = 'uaqs_files';
  $configuration->entity_type = 'file';
  $configuration->entity_type_bundles = array();
  $configuration->button_icon_fid = 0;
  $configuration->display_plugins = array(
    'file_entity:file_rendered' => 'file_entity:file_rendered',
  );
  $export['uaqs_files'] = $configuration;

  return $export;
}

/**
 * Implements hook_default_entity_embed_configuration_alter().
 *
 * This function sets the button icon for entity_embed.
 * @see https://www.drupal.org/node/2771149
 */
function uaqs_fields_default_entity_embed_configuration_alter(&$configurations) {
  $icon_files = array('uaqs_files','uaqs_content','uaqs_bean');
  foreach ($icon_files as $icon_file) {
    if (!empty($configurations[$icon_file])) {
      $configurations[$icon_file]->button_icon_fid = variable_get('uaqs_fields_entity_embed_' . $icon_file . '_fid', 0);
    }
  }
}